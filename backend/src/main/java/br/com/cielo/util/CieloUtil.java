package br.com.cielo.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CieloUtil {

	
	public static Date stringToDate(final String date){
		Date data = new Date();
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			data = format.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return data;
	}
	
	public static String dateToString(final Date date){
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(date);
	}
	
}

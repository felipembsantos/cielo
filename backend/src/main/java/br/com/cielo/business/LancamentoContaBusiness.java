package br.com.cielo.business;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.cielo.converter.LancamentoConverter;
import br.com.cielo.dto.LancamentoContaDTO;
import br.com.cielo.entity.Lancamento;
import br.com.cielo.service.LancamentoContaService;
import br.com.cielo.vo.LancamentoContaVO;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

@Service
public class LancamentoContaBusiness implements LancamentoContaService{

	public List<LancamentoContaVO> updateDataLegado(LancamentoContaDTO lancamentoContaDTO) {
		Lancamento lancamento = LancamentoConverter.converterDTOtoEntity(lancamentoContaDTO);
		
		//Aqui apenas para exemplificar, � poss�vel realizar as implementa��es abaixo:
		//1 - Interagir com o banco de dados
		//2 - Chamar servi�os terceiros, rest/soa
		//3 - Interagir com outros business
		//4 - Implementar regras de neg�cio
		
		return LancamentoConverter.converterEntityToVOList(lancamento);
	}
}

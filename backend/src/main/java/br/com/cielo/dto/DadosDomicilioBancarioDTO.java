package br.com.cielo.dto;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

public class DadosDomicilioBancarioDTO {

	private String codigoBanco;
	
	private int numeroAgencia;
	
	private String numeroContaCorrente;

	public String getCodigoBanco() {
		return codigoBanco;
	}

	public void setCodigoBanco(String codigoBanco) {
		this.codigoBanco = codigoBanco;
	}

	public int getNumeroAgencia() {
		return numeroAgencia;
	}

	public void setNumeroAgencia(int numeroAgencia) {
		this.numeroAgencia = numeroAgencia;
	}

	public String getNumeroContaCorrente() {
		return numeroContaCorrente;
	}

	public void setNumeroContaCorrente(String numeroContaCorrente) {
		this.numeroContaCorrente = numeroContaCorrente;
	}
	
	
}

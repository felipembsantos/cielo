package br.com.cielo.dto;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

public class TotalControleLancamentoDTO {

	private int quantidadeLancamentos;
	
	private int quantidadeRemessas;
	
	private float valorLancamentos;

	public int getQuantidadeLancamentos() {
		return quantidadeLancamentos;
	}

	public void setQuantidadeLancamentos(int quantidadeLancamentos) {
		this.quantidadeLancamentos = quantidadeLancamentos;
	}

	public int getQuantidadeRemessas() {
		return quantidadeRemessas;
	}

	public void setQuantidadeRemessas(int quantidadeRemessas) {
		this.quantidadeRemessas = quantidadeRemessas;
	}

	public float getValorLancamentos() {
		return valorLancamentos;
	}

	public void setValorLancamentos(float valorLancamentos) {
		this.valorLancamentos = valorLancamentos;
	}
	
}

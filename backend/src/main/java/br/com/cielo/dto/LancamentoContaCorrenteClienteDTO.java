package br.com.cielo.dto;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

public class LancamentoContaCorrenteClienteDTO {

	private String numeroRemessaBanco;
	
	private String nomeSituacaoRemessa;

	private String nomeTipoOperacao;
	
	private DadosDomicilioBancarioDTO dadosDomicilioBancario;
	
	public String getNumeroRemessaBanco() {
		return numeroRemessaBanco;
	}

	public void setNumeroRemessaBanco(String numeroRemessaBanco) {
		this.numeroRemessaBanco = numeroRemessaBanco;
	}

	public String getNomeSituacaoRemessa() {
		return nomeSituacaoRemessa;
	}

	public void setNomeSituacaoRemessa(String nomeSituacaoRemessa) {
		this.nomeSituacaoRemessa = nomeSituacaoRemessa;
	}

	public String getNomeTipoOperacao() {
		return nomeTipoOperacao;
	}

	public void setNomeTipoOperacao(String nomeTipoOperacao) {
		this.nomeTipoOperacao = nomeTipoOperacao;
	}

	public DadosDomicilioBancarioDTO getDadosDomicilioBancario() {
		return dadosDomicilioBancario;
	}

	public void setDadosDomicilioBancario(DadosDomicilioBancarioDTO dadosDomicilioBancario) {
		this.dadosDomicilioBancario = dadosDomicilioBancario;
	}

}

package br.com.cielo.dto;

import java.util.List;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

public class LancamentoContaDTO {

	private TotalControleLancamentoDTO totalControleLancamento;
	
	private List<ControleLancamentoDTO> controleLancamentoList;
	
	private int indice;
	
	private int tamanhoPagina;
	
	private int totalElements;

	public TotalControleLancamentoDTO getTotalControleLancamento() {
		return totalControleLancamento;
	}

	public void setTotalControleLancamento(TotalControleLancamentoDTO totalControleLancamento) {
		this.totalControleLancamento = totalControleLancamento;
	}

	public List<ControleLancamentoDTO> getControleLancamentoList() {
		return controleLancamentoList;
	}

	public void setControleLancamentoList(List<ControleLancamentoDTO> controleLancamentoList) {
		this.controleLancamentoList = controleLancamentoList;
	}

	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}

	public int getTamanhoPagina() {
		return tamanhoPagina;
	}

	public void setTamanhoPagina(int tamanhoPagina) {
		this.tamanhoPagina = tamanhoPagina;
	}

	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
}

package br.com.cielo.converter;

import java.util.ArrayList;
import java.util.List;

import br.com.cielo.dto.LancamentoContaDTO;
import br.com.cielo.entity.Lancamento;
import br.com.cielo.entity.LancamentoDatail;
import br.com.cielo.util.CieloUtil;
import br.com.cielo.vo.LancamentoContaVO;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

public class LancamentoConverter {

	
	public static Lancamento converterDTOtoEntity(final LancamentoContaDTO lancamentoContaDTO){
		Lancamento lancamento = new Lancamento();
		if(lancamentoContaDTO != null){
			lancamento.setCodigo(1);
			lancamento.setIndice(lancamentoContaDTO.getIndice());
			lancamento.setLancamentoDatailList(LancamentoDatailConverter.convertDTOToEntityList(lancamentoContaDTO.getControleLancamentoList()));
			lancamento.setTamanhoPagina(lancamentoContaDTO.getTamanhoPagina());
			lancamento.setTotalElements(lancamentoContaDTO.getTotalElements());
			
			if(lancamentoContaDTO.getTotalControleLancamento() != null){
				lancamento.setQuantidadeLancamentos(lancamentoContaDTO.getTotalControleLancamento().getQuantidadeLancamentos());
				lancamento.setQuantidadeRemessas(lancamentoContaDTO.getTotalControleLancamento().getQuantidadeRemessas());
				lancamento.setValorLancamentos(lancamentoContaDTO.getTotalControleLancamento().getValorLancamentos());
			}
			
		}
		return lancamento;
	}
	
	public static List<LancamentoContaVO> converterEntityToVOList(final Lancamento lancamento){
		List<LancamentoContaVO> lancamentoContaVOList = new ArrayList<LancamentoContaVO>();
		if(lancamento != null){
			List<LancamentoDatail> lancamentoDatailList = lancamento.getLancamentoDatailList();
			
			if(!lancamentoDatailList.isEmpty()){
				for (LancamentoDatail lancamentoDatail : lancamentoDatailList) {
					if(lancamentoDatail != null){
						LancamentoContaVO lancamentoContaVO = new LancamentoContaVO();
						lancamentoContaVO.setDadosBancarios(converterDadosBancarios(lancamentoDatail));
						lancamentoContaVO.setDataConfirmacao(CieloUtil.dateToString(lancamentoDatail.getDataEfetivaLancamento()));
						lancamentoContaVO.setDataLancamento(CieloUtil.dateToString(lancamentoDatail.getDataLancamentoContaCorrenteCliente()));
						lancamentoContaVO.setDescricao(lancamentoDatail.getNomeTipoOperacao());
						lancamentoContaVO.setNumero(lancamentoDatail.getNumeroRemessaBanco());
						lancamentoContaVO.setSituacao(lancamentoDatail.getNomeSituacaoRemessa());
						lancamentoContaVO.setValorFinal(converterValorFinal(lancamentoDatail.getValorLancamentoRemessa()));
						lancamentoContaVOList.add(lancamentoContaVO);
					}
				}
			}
		}
		return lancamentoContaVOList;
	}
	
	public static String converterDadosBancarios(final LancamentoDatail lancamentoDatail){
		StringBuilder dadosBancarios = new StringBuilder();
		dadosBancarios.append(lancamentoDatail.getNomeBanco());
		dadosBancarios.append("Ag ");
		dadosBancarios.append(lancamentoDatail.getNumeroAgencia());
		dadosBancarios.append("CC ");
		dadosBancarios.append(lancamentoDatail.getNumeroContaCorrente());
		return dadosBancarios.toString();
	}
	
	public static String converterValorFinal(final float valorLancamentoRemessa){
		StringBuilder valorFinal = new StringBuilder();
		valorFinal.append("R$ ");
		valorFinal.append(valorLancamentoRemessa);
		return valorFinal.toString();
	}
}

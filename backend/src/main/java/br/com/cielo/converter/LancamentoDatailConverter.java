package br.com.cielo.converter;

import java.util.ArrayList;
import java.util.List;

import br.com.cielo.dto.ControleLancamentoDTO;
import br.com.cielo.dto.DadosDomicilioBancarioDTO;
import br.com.cielo.entity.LancamentoDatail;
import br.com.cielo.util.CieloUtil;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

public class LancamentoDatailConverter {
	
	public static List<LancamentoDatail> convertDTOToEntityList(final List<ControleLancamentoDTO> controleLancamentoDTOList){
		List<LancamentoDatail> lancamentoDatailList = new ArrayList<LancamentoDatail>();
		if(controleLancamentoDTOList != null){
			for (ControleLancamentoDTO controleLancamentoDTO : controleLancamentoDTOList) {
				lancamentoDatailList.add(convertDTOtoEntity(controleLancamentoDTO));
			}
		}
		return lancamentoDatailList;
	}
	
	public static LancamentoDatail convertDTOtoEntity(final ControleLancamentoDTO controleLancamentoDTO){
		LancamentoDatail lancamentoDatail = new LancamentoDatail();
		if(controleLancamentoDTO != null){
			
			if((controleLancamentoDTO.getLancamentoContaCorrenteCliente() != null)){
				
				lancamentoDatail.setNumeroRemessaBanco(controleLancamentoDTO.getLancamentoContaCorrenteCliente().getNumeroRemessaBanco());
				lancamentoDatail.setNomeSituacaoRemessa(controleLancamentoDTO.getLancamentoContaCorrenteCliente().getNomeSituacaoRemessa());
				lancamentoDatail.setNomeTipoOperacao(controleLancamentoDTO.getLancamentoContaCorrenteCliente().getNomeTipoOperacao());
				
				if(controleLancamentoDTO.getLancamentoContaCorrenteCliente().getDadosDomicilioBancario() != null){
					DadosDomicilioBancarioDTO dadosDomicilioBancarioDTO = controleLancamentoDTO.getLancamentoContaCorrenteCliente().getDadosDomicilioBancario();
					lancamentoDatail.setCodigoBanco(dadosDomicilioBancarioDTO.getCodigoBanco());
					lancamentoDatail.setNumeroAgencia(dadosDomicilioBancarioDTO.getNumeroAgencia());
					lancamentoDatail.setNumeroContaCorrente(dadosDomicilioBancarioDTO.getNumeroContaCorrente());
				}
			}
			
			lancamentoDatail.setDataEfetivaLancamento(CieloUtil.stringToDate(controleLancamentoDTO.getDataEfetivaLancamento()));
			lancamentoDatail.setDataLancamentoContaCorrenteCliente(CieloUtil.stringToDate(controleLancamentoDTO.getDataLancamentoContaCorrenteCliente()));
			lancamentoDatail.setDescricaoGrupoPagamento(controleLancamentoDTO.getDescricaoGrupoPagamento());
			lancamentoDatail.setNomeBanco(controleLancamentoDTO.getNomeBanco());
			lancamentoDatail.setNumeroEvento(controleLancamentoDTO.getNumeroEvento());
			lancamentoDatail.setNumeroRaizCNPJ(controleLancamentoDTO.getNumeroRaizCNPJ());
			lancamentoDatail.setNumeroSufixoCNPJ(controleLancamentoDTO.getNumeroSufixoCNPJ());
			lancamentoDatail.setQuantidadeLancamentoRemessa(controleLancamentoDTO.getQuantidadeLancamentoRemessa());
			lancamentoDatail.setValorLancamentoRemessa(controleLancamentoDTO.getValorLancamentoRemessa());
		}
		return lancamentoDatail;
	}

}

package br.com.cielo.service;

import java.util.List;

import br.com.cielo.dto.LancamentoContaDTO;
import br.com.cielo.vo.LancamentoContaVO;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

public interface LancamentoContaService {
	
	public List<LancamentoContaVO> updateDataLegado(final LancamentoContaDTO lancamentoContaDTO);

}

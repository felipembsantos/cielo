package br.com.cielo.entity;

import java.util.List;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

public class Lancamento {

	private int codigo;
	
	private int quantidadeLancamentos;
	
	private int quantidadeRemessas;
	
	private float valorLancamentos;
	
	private int indice;
	
	private int tamanhoPagina;
	
	private int totalElements;
	
	private List<LancamentoDatail> lancamentoDatailList;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getQuantidadeLancamentos() {
		return quantidadeLancamentos;
	}

	public void setQuantidadeLancamentos(int quantidadeLancamentos) {
		this.quantidadeLancamentos = quantidadeLancamentos;
	}

	public int getQuantidadeRemessas() {
		return quantidadeRemessas;
	}

	public void setQuantidadeRemessas(int quantidadeRemessas) {
		this.quantidadeRemessas = quantidadeRemessas;
	}

	public float getValorLancamentos() {
		return valorLancamentos;
	}

	public void setValorLancamentos(float valorLancamentos) {
		this.valorLancamentos = valorLancamentos;
	}

	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}

	public int getTamanhoPagina() {
		return tamanhoPagina;
	}

	public void setTamanhoPagina(int tamanhoPagina) {
		this.tamanhoPagina = tamanhoPagina;
	}

	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	public List<LancamentoDatail> getLancamentoDatailList() {
		return lancamentoDatailList;
	}

	public void setLancamentoDatailList(List<LancamentoDatail> lancamentoDatailList) {
		this.lancamentoDatailList = lancamentoDatailList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lancamento other = (Lancamento) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}
}

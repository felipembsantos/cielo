package br.com.cielo.entity;

import java.util.Date;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

public class LancamentoDatail {

	private int codigo;
	
	private String numeroRemessaBanco;
	
	private String nomeSituacaoRemessa;
	
	private String codigoBanco;
	
	private int numeroAgencia;
	
	private String numeroContaCorrente;
	
	private String nomeTipoOperacao;
	
	private Date dataEfetivaLancamento;
	
	private Date dataLancamentoContaCorrenteCliente;
	
	private int numeroEvento;
	
	private String descricaoGrupoPagamento;
	
	private int codigoIdentificadorUnico;
	
	private String nomeBanco;
	
	private int quantidadeLancamentoRemessa;
	
	private String numeroRaizCNPJ;
	
	private String numeroSufixoCNPJ;
	
	private float valorLancamentoRemessa;
	
	private Date dateLancamentoContaCorrenteCliente;
	
	private Date dateEfetivaLancamento;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNumeroRemessaBanco() {
		return numeroRemessaBanco;
	}
	
	public void setNumeroRemessaBanco(String numeroRemessaBanco) {
		this.numeroRemessaBanco = numeroRemessaBanco;
	}

	public String getNomeSituacaoRemessa() {
		return nomeSituacaoRemessa;
	}

	public void setNomeSituacaoRemessa(String nomeSituacaoRemessa) {
		this.nomeSituacaoRemessa = nomeSituacaoRemessa;
	}

	public String getCodigoBanco() {
		return codigoBanco;
	}

	public void setCodigoBanco(String codigoBanco) {
		this.codigoBanco = codigoBanco;
	}

	public int getNumeroAgencia() {
		return numeroAgencia;
	}

	public void setNumeroAgencia(int numeroAgencia) {
		this.numeroAgencia = numeroAgencia;
	}

	public String getNumeroContaCorrente() {
		return numeroContaCorrente;
	}

	public void setNumeroContaCorrente(String numeroContaCorrente) {
		this.numeroContaCorrente = numeroContaCorrente;
	}

	public String getNomeTipoOperacao() {
		return nomeTipoOperacao;
	}

	public void setNomeTipoOperacao(String nomeTipoOperacao) {
		this.nomeTipoOperacao = nomeTipoOperacao;
	}

	public Date getDataEfetivaLancamento() {
		return dataEfetivaLancamento;
	}

	public void setDataEfetivaLancamento(Date dataEfetivaLancamento) {
		this.dataEfetivaLancamento = dataEfetivaLancamento;
	}

	public Date getDataLancamentoContaCorrenteCliente() {
		return dataLancamentoContaCorrenteCliente;
	}

	public void setDataLancamentoContaCorrenteCliente(Date dataLancamentoContaCorrenteCliente) {
		this.dataLancamentoContaCorrenteCliente = dataLancamentoContaCorrenteCliente;
	}

	public int getNumeroEvento() {
		return numeroEvento;
	}

	public void setNumeroEvento(int numeroEvento) {
		this.numeroEvento = numeroEvento;
	}

	public String getDescricaoGrupoPagamento() {
		return descricaoGrupoPagamento;
	}

	public void setDescricaoGrupoPagamento(String descricaoGrupoPagamento) {
		this.descricaoGrupoPagamento = descricaoGrupoPagamento;
	}

	public int getCodigoIdentificadorUnico() {
		return codigoIdentificadorUnico;
	}

	public void setCodigoIdentificadorUnico(int codigoIdentificadorUnico) {
		this.codigoIdentificadorUnico = codigoIdentificadorUnico;
	}

	public String getNomeBanco() {
		return nomeBanco;
	}

	public void setNomeBanco(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}

	public int getQuantidadeLancamentoRemessa() {
		return quantidadeLancamentoRemessa;
	}

	public void setQuantidadeLancamentoRemessa(int quantidadeLancamentoRemessa) {
		this.quantidadeLancamentoRemessa = quantidadeLancamentoRemessa;
	}

	public String getNumeroRaizCNPJ() {
		return numeroRaizCNPJ;
	}

	public void setNumeroRaizCNPJ(String numeroRaizCNPJ) {
		this.numeroRaizCNPJ = numeroRaizCNPJ;
	}

	public String getNumeroSufixoCNPJ() {
		return numeroSufixoCNPJ;
	}

	public void setNumeroSufixoCNPJ(String numeroSufixoCNPJ) {
		this.numeroSufixoCNPJ = numeroSufixoCNPJ;
	}

	public float getValorLancamentoRemessa() {
		return valorLancamentoRemessa;
	}

	public void setValorLancamentoRemessa(float valorLancamentoRemessa) {
		this.valorLancamentoRemessa = valorLancamentoRemessa;
	}

	public Date getDateLancamentoContaCorrenteCliente() {
		return dateLancamentoContaCorrenteCliente;
	}

	public void setDateLancamentoContaCorrenteCliente(Date dateLancamentoContaCorrenteCliente) {
		this.dateLancamentoContaCorrenteCliente = dateLancamentoContaCorrenteCliente;
	}

	public Date getDateEfetivaLancamento() {
		return dateEfetivaLancamento;
	}

	public void setDateEfetivaLancamento(Date dateEfetivaLancamento) {
		this.dateEfetivaLancamento = dateEfetivaLancamento;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		result = prime * result + numeroEvento;
		result = prime * result + ((numeroRaizCNPJ == null) ? 0 : numeroRaizCNPJ.hashCode());
		result = prime * result + ((numeroSufixoCNPJ == null) ? 0 : numeroSufixoCNPJ.hashCode());
		result = prime * result + Float.floatToIntBits(valorLancamentoRemessa);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LancamentoDatail other = (LancamentoDatail) obj;
		if (codigo != other.codigo)
			return false;
		if (numeroEvento != other.numeroEvento)
			return false;
		if (numeroRaizCNPJ == null) {
			if (other.numeroRaizCNPJ != null)
				return false;
		} else if (!numeroRaizCNPJ.equals(other.numeroRaizCNPJ))
			return false;
		if (numeroSufixoCNPJ == null) {
			if (other.numeroSufixoCNPJ != null)
				return false;
		} else if (!numeroSufixoCNPJ.equals(other.numeroSufixoCNPJ))
			return false;
		if (Float.floatToIntBits(valorLancamentoRemessa) != Float.floatToIntBits(other.valorLancamentoRemessa))
			return false;
		return true;
	}


}

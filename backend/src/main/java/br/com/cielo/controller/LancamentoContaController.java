package br.com.cielo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cielo.dto.LancamentoContaDTO;
import br.com.cielo.service.LancamentoContaService;
import br.com.cielo.vo.LancamentoContaVO;

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

@RestController
@RequestMapping("/v1/lancamento/conta")
public class LancamentoContaController {

	@Autowired
    private LancamentoContaService lancamentoContaService;
	
	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "http://localhost")
	@PostMapping(value = "/updateolddata", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LancamentoContaVO>> updateDataLegado(@RequestBody LancamentoContaDTO lancamentoContaDTO){
		return ResponseEntity.ok().body(lancamentoContaService.updateDataLegado(lancamentoContaDTO));
	}
}

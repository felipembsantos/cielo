/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

(function () {
	'use strict'
	angular.module('cielo').factory('LancamentoContaFactory', LancamentoContaFactory);
	
	function LancamentoContaFactory() {
		return {
			convertLancamentoContaLegado: convertLancamentoContaLegado,
			convertLancamentoConta: convertLancamentoConta 
		}
	}
	
	function LancamentoConta(lancamentoConta){	
		var lancamentoConta = {		
			totalControleLancamento : new TotalControleLancamento(lancamentoConta.totalControleLancamento),
			controleLancamentoList : convertControleLancamentoList(lancamentoConta.listaControleLancamento),
			indice : lancamentoConta.indice,
			tamanhoPagina : lancamentoConta.tamanhoPagina,
			totalElements : lancamentoConta.totalElements,
		};
		return lancamentoConta;
	}
	
	function TotalControleLancamento(totalControleLancamento){
		var totalControleLancamento = {
			quantidadeLancamentos : totalControleLancamento.quantidadeLancamentos,
			quantidadeRemessas : totalControleLancamento.quantidadeRemessas,
			valorLancamentos : totalControleLancamento.valorLancamentos
		};
		return totalControleLancamento;
	}
	
	function ControleLancamento(controleLancamento){
		var controleLancamento = {
			lancamentoContaCorrenteCliente: new LancamentoContaCorrenteCliente(controleLancamento.lancamentoContaCorrenteCliente),
			dataEfetivaLancamento: controleLancamento.dataEfetivaLancamento,
			dataLancamentoContaCorrenteCliente: controleLancamento.dataLancamentoContaCorrenteCliente,
			numeroEvento: controleLancamento.numeroEvento,
			descricaoGrupoPagamento: controleLancamento.descricaoGrupoPagamento,
			codigoIdentificadorUnico: controleLancamento.codigoIdentificadorUnico,
			nomeBanco: controleLancamento.nomeBanco,
			quantidadeLancamentoRemessa: controleLancamento.quantidadeLancamentoRemessa,
			numeroRaizCNPJ: controleLancamento.numeroRaizCNPJ,
			numeroSufixoCNPJ: controleLancamento.numeroSufixoCNPJ,
			valorLancamentoRemessa: controleLancamento.valorLancamentoRemessa,
			dateLancamentoContaCorrenteCliente: controleLancamento.dateLancamentoContaCorrenteCliente,
			dateEfetivaLancamento: controleLancamento.dateEfetivaLancamento
		};
		return controleLancamento;
	}
	
	function LancamentoContaCorrenteCliente(lancamentoContaCorrenteCliente){
		var lancamentoContaCorrenteCliente = {
			numeroRemessaBanco : lancamentoContaCorrenteCliente.numeroRemessaBanco,
			nomeSituacaoRemessa : lancamentoContaCorrenteCliente.nomeSituacaoRemessa,
			dadosDomicilioBancario : new DadosDomicilioBancario(lancamentoContaCorrenteCliente.dadosDomicilioBancario),
			nomeTipoOperacao : lancamentoContaCorrenteCliente.nomeTipoOperacao
		};
		return lancamentoContaCorrenteCliente;
	}
	
	function DadosDomicilioBancario(dadosDocimilioBancario){
		var dadosDocimilioBancario = {
			codigoBanco: dadosDocimilioBancario.codigoBanco,
			numeroAgencia: dadosDocimilioBancario.numeroAgencia,
			numeroContaCorrente: dadosDocimilioBancario.numeroContaCorrente,
		};
		return dadosDocimilioBancario;
	}

	function convertLancamentoContaLegado(lancamentoContaLegado) {
		return new LancamentoConta(lancamentoContaLegado);
	}
	
	function convertLancamentoConta(lancamentoContaList) {
		var lancamentoContaListConverted = [];
		for (var i = 0; i < lancamentoContaList.length; ++i) {
			lancamentoContaListConverted.push(new Lancamento(lancamentoContaList[i]));
		}
		return lancamentoContaListConverted;
	}
	
	function Lancamento(lancamento){
		var lancamento = {
			codigo : 1,
			dataLancamento: lancamento.dataLancamento,
			descricao: lancamento.descricao,
			numero: lancamento.numero,
			situacao: lancamento.situacao,
			dataConfirmacao: lancamento.dataConfirmacao,
			dadosBancarios: lancamento.dadosBancarios,
			valorFinal: lancamento.valorFinal
		};
		return lancamento;
	}
	
	function convertControleLancamentoList(controleLancamentoList){
		var controleLancamentoListConverted = [];
		for (var i = 0; i < controleLancamentoList.length; ++i) {
			controleLancamentoListConverted.push(new ControleLancamento(controleLancamentoList[i]));
		}
		return controleLancamentoListConverted;
	}

})();

/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

(function () {
    'use strict'
    angular.module('cielo').controller('LancamentoContaController', LancamentoContaController);
    
    LancamentoContaController.$inject = ['$scope', '$rootScope', '$filter', 'LancamentoContaFactory', 'LancamentoContaService'];
    
    function LancamentoContaController($scope, $rootScope, $filter, LancamentoContaFactory, LancamentoContaService) {
        
		var vm = this;
		
		//Methods
		vm.loadLancamentoConta = loadLancamentoConta;
		
		//List
		vm.lancamentoContaList = [];
			
		//Object View
		vm.lancamentoConta = {};
		           
		function init() {
			loadLancamentoConta();
		}

		function loadLancamentoConta(){
			LancamentoContaService.findAll().then(function (response) {
				if(!angular.isUndefined(response)){
					if(!angular.isUndefined(response.data)){
						var lancamentoLegado = LancamentoContaFactory.convertLancamentoContaLegado(response.data);
						LancamentoContaService.updateOldData(lancamentoLegado).then(function (response) {
							if(!angular.isUndefined(response)){
								if(!angular.isUndefined(response.data)){
									vm.lancamentoContaList = response.data;
								}
							}
						});
					}
				}
            });	
		}
			
		init();
    }
})();
/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

(function () {
	'use strict';

    angular.module('cielo').service('LancamentoContaService', LancamentoContaService);
    LancamentoContaService.$inject = ['$http'];
   
    function LancamentoContaService($http) {
        
		var vm = this;       
        var _headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

		vm.findAll = findAll;
		vm.updateOldData = updateOldData;

        function findAll(){
			var URL = 'http://localhost/cielo/projeto/modules/lancamentoconta/mock/lancamento-conta-legado.json';
            return $http.get(URL, {headers: _headers});
        }
		
		function updateOldData(lancamentoConta){
			var URL = 'http://localhost:8080/v1/lancamento/conta/updateolddata';
            return $http.post(URL, lancamentoConta);
		}
    }
})();
/*
 * @author: felipemirandadev@yahoo.com.br
 * @since: 03/01/2019
 */

(function() {
    angular.module('cielo', ['ui.router', 'ui.bootstrap', 'oc.lazyLoad', 'ui.mask', 'ui.utils.masks']);
    
	angular.module('cielo').config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 
		function($stateProvider, $urlRouterProvider, $httpProvider) {

			$urlRouterProvider.otherwise('/');
			
			 $stateProvider
				.state('lancamentoconta', {
				  url: '/lancamento-conta',
				  templateUrl: 'projeto/modules/lancamentoconta/template/lancamento-conta.html',
				    resolve: {
								loadDeps: ['$ocLazyLoad', function($ocLazyLoad) {
									return $ocLazyLoad.load([
										'projeto/modules/lancamentoconta/factory/lancamento-conta.factory.js',
										'projeto/modules/lancamentoconta/service/lancamento-conta.service.js',
										'projeto/modules/lancamentoconta/controller/lancamento-conta.controller.js'
									]);
								}]
					}
				});

		}
	]);

})();
